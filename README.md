# COINBASE-CLI
### Command line interface to the [coinbase](https://github.com/vpenkoff/coinbase) library.
### Usage:
```./coinbase-cli -api=orders -command=createNewOrder -data='{"size":"0.01","price":"0.100", "side":"buy", "product_id":"ETH-BTC"}'  -c=/home/joe/configs/coinbase.json```

***

##Todo:
1. Better docs
2. Tests
