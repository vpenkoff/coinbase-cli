package main

import (
	"flag"
	"github.com/vpenkoff/coinbase"
)

func main() {
	apiPtr := flag.String("api", "foo", "The api that we gonna call")
	commandPtr := flag.String("command", "foo", "Command to send to coinbase - one of getProductTicker, createNewOrder")
	requestDataPtr := flag.String("data", "", "The data to send")
	configPtr := flag.String("config", "", "Path to the config file")

	flag.Parse()

	coinbase.CallApi(apiPtr, commandPtr, requestDataPtr, configPtr)
}
